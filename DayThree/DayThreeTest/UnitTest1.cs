using DayThree;
using System;
using System.Collections.Generic;
using Xunit;

namespace DayThreeTest
{
    public class UnitTest1
    {
        [Fact]
        public void PuzzleOneTest()
        {
            List<string> diagnosticReport = new List<string>() { 
                "00100",
                "11110",
                "10110",
                "10111",
                "10101",
                "01111",
                "00111",
                "11100",
                "10000",
                "11001",
                "00010",
                "01010" 
            };

            int result = Logic.Diagnostic(diagnosticReport);
            int expectedResult = 198;

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void PuzzleTwoTest()
        {
            List<string> diagnosticReport = new List<string>() {
                "00100",
                "11110",
                "10110",
                "10111",
                "10101",
                "01111",
                "00111",
                "11100",
                "10000",
                "11001",
                "00010",
                "01010"
            };

            int result = Logic.LifeSupport(diagnosticReport);
            int expectedResult = 230;

            Assert.Equal(expectedResult, result);
        }
    }
}

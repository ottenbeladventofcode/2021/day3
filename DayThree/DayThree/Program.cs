﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DayThree
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                if (args.Length != 2) 
                { 
                    throw new Exception("This program takes two inputs the first denoting which algorithm to use and the second the file path of a file containing a list of measurements"); 
                }

                int algorithm = int.Parse(args[0]); 
                string[] inputs = System.IO.File.ReadAllLines(args[1]); 
                List<string> diagnosticReport = new List<string>(); 
                for (int i = 0; i < inputs.Length; i++) 
                {
                    diagnosticReport.Add(inputs[i]);
                }

                DateTime start = DateTime.Now; 
                int result = -1;

                if (algorithm == 0)
                {
                    result = Logic.Diagnostic(diagnosticReport);
                }
                else
                {
                    result = Logic.LifeSupport(diagnosticReport);
                }

                DateTime end = DateTime.Now; 
                Console.WriteLine(string.Format("The result is {0}.", result)); 
                Console.WriteLine(end.Subtract(start).ToString());
            }
            catch (Exception)
            {
                Console.WriteLine("Unexpected error");
            }
        }
    }

    public static class Logic
    {
        public static int Diagnostic(List<string> diagnoticReport)
        {
            List<BitCounter> bitCounters = new List<BitCounter>();

            for (int i = 0; i < diagnoticReport[0].Length; i++)
            {
                bitCounters.Add(new BitCounter());
            }

            for (int i = 0; i < diagnoticReport.Count; i++)
            {
                for (int j = 0; j < diagnoticReport[i].Length; j++)
                {
                    if (diagnoticReport[i][j] == '0')
                    {
                        bitCounters[j].Zeros++;
                    }
                    else
                    {
                        bitCounters[j].Ones++;
                    }
                }
            }

            string gammaRate = "";
            string epsilonRate = "";
            
            for(int i = 0; i < bitCounters.Count; i++)
            {
                if (bitCounters[i].Zeros > bitCounters[i].Ones)
                {
                    gammaRate += "0";
                    epsilonRate += "1";
                }
                else if (bitCounters[i].Zeros < bitCounters[i].Ones)
                {
                    gammaRate += "1";
                    epsilonRate += "0";
                }
                else
                {
                    throw new Exception("How do we canculate gamma and epsilon when there are an equal number of bits?");
                }
            }

            int gammaInt = Convert.ToInt32(gammaRate, 2);
            int epsilonInt = Convert.ToInt32(epsilonRate, 2);

            return gammaInt * epsilonInt;
        }

        public static int LifeSupport(List<string> diagnoticReport)
        {
            List<string> oxygenReport = new List<string>();
            List<string> co2Report = new List<string>();

            oxygenReport.AddRange(diagnoticReport);
            co2Report.AddRange(diagnoticReport);

            int oxygenRating = CalculateReport(oxygenReport, true);
            int co2Rating = CalculateReport(co2Report, false);

            return oxygenRating * co2Rating;
        }

        public static int CalculateReport(List<string> input, bool significance)
        {
            for (int k = 0; k < input[0].Length; k++)
            {
                List<BitCounter> bitCounters = new List<BitCounter>();
                for (int i = 0; i < input[0].Length; i++)
                {
                    bitCounters.Add(new BitCounter());
                }

                for (int i = 0; i < input.Count; i++)
                {
                    for (int j = 0; j < input[i].Length; j++)
                    {
                        if (input[i][j] == '0')
                        {
                            bitCounters[j].Zeros++;
                        }
                        else
                        {
                            bitCounters[j].Ones++;
                        }
                    }
                }

                char currentBit;
               
                if (significance) //Most common bit
                {
                    if (bitCounters[k].Ones >= bitCounters[k].Zeros )
                    {
                        currentBit = '1';
                    }
                    else
                    {
                        currentBit = '0';
                    }
                }
                else //Least common bit
                {
                    if (bitCounters[k].Ones >= bitCounters[k].Zeros)
                    {
                        currentBit = '0';
                    }
                    else
                    {
                        currentBit = '1';
                    }
                }

                input = input.Where(x => x[k] == currentBit).ToList();

                if (input.Count == 1)
                {
                    break;
                }
            }

            return Convert.ToInt32(input.First(), 2);
        }
    }

    public class BitCounter
    {
        public int Zeros { get; set; }
        public int Ones { get; set; }
    }
}
